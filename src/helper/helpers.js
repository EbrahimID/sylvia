import $ from "jquery";
import _ from "underscore";

var pageWidth, pageHeight;

var basePage = {
  width: 600,
  height: 513,
  scale: 1,
  scaleX: 1,
  scaleY: 1
};

export default function(baseWidth, baseHeight, resize = true) {
  basePage.width = baseWidth;
  basePage.height = baseHeight;
  var $page = $(".page_content");
  getPageSize();
  scalePages($page, pageWidth, pageHeight);
  console.log(pageWidth);
  //using underscore to delay resize method till finished resizing window
  if (resize) {
    $(window).resize(
      _.debounce(function() {
        getPageSize();
        scalePages($page, pageWidth, pageHeight);
      }, 150)
    );
  }

  function getPageSize() {
    pageHeight = $("#container").height();
    pageWidth = $("#container").width();
  }

  function scalePages(page, maxWidth, maxHeight) {
    var scaleX = 1,
      scaleY = 1;
    scaleX = maxWidth / basePage.width;
    scaleY = maxHeight / basePage.height;
    basePage.scaleX = scaleX;
    basePage.scaleY = scaleY;
    basePage.scale = scaleX > scaleY ? scaleY : scaleX;

    page.attr(
      "style",
      "-webkit-transform:scale(" +
        basePage.scale +
        ") translate(-50%,-50%); position: absolute; top:50%; left:50%;"
    );
  }
}
