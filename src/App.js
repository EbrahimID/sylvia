import React from "react";
import "./App.css";
import { useParams, Link } from "react-router-dom";
import Music from "./components/Music";

function App() {
  return (
    <>
      <div className="App">
        <div className="display">
          <img
            src={require("./assets/Blank 8 x 4 in (1).png")}
            alt="kids"
            width="400px"
            height="200px"
          ></img>
          <Link to="/BasicInfo" className="enter">
            <h1>Enter</h1>
          </Link>
        </div>
        <Music src="1-Disney Opening Theme Tutorial [Synthesia].mp3" />
      </div>
    </>
  );
}

export default App;
