import React from "react";

function Program() {
  return (
    <>
      <div className="background">
        <img
          className="center"
          src={`http://hbsocialcommittee.com/assets/EntPr.png`}
          alt="Chrismas card"
          height="100%"
          width="auto"
        ></img>
      </div>
      <div className="App">
        <iframe title="my iframe" allow="autoplay" id="audio"></iframe>
        <audio src="/assets/NatKing.mp3" controls autoPlay />
      </div>
    </>
  );
}

export default Program;
