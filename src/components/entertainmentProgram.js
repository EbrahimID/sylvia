import React, { Component } from "react";
import Music from "./Music";
import helpers from "../helper/helpers";

export default class EntertainmentProgram extends Component {
  componentDidMount() {
    helpers(1700, 800);
  }
  render() {
    return (
      <>
        <div id="container">
          <div className="page_content">
            <img
              src={require("../assets/entProImage.png")}
              alt="basic info"
            ></img>
            <img
              className="GrinchHand"
              src={require("../assets/GrinchHand.png")}
              alt="grinch hand"
            ></img>
            <img
              className="GrinchHand2"
              src={require("../assets/GrinchHand.png")}
              alt="grinch hand"
            ></img>
          </div>
          <Music src="4-Entertainment Program.mp3" />
        </div>
      </>
    );
  }
}
