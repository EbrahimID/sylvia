import React, { Component } from "react";
import Music from "./Music";
import helpers from "../helper/helpers";

export default class InvitationDetail extends Component {
  componentDidMount() {
    helpers(1098, 565);
  }
  render() {
    return (
      <>
        <div id="container">
          <div className="page_content">
            <img
              src={require("../assets/InvDetailImgae.jpg")}
              alt="basic info"
            ></img>
          </div>
          <Music src="3-Eartha Kitt - Santa Baby (Audio).mp3" />
        </div>
      </>
    );
  }
}
