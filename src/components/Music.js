import React from "react";

function Music(props) {
  console.log(props);
  let finalSrc = require(`../assets/${props.src}`);
  return (
    <div className="music">
      <iframe title="my iframe" allow="autoplay" id="audio"></iframe>
      <audio src={finalSrc} controls autoPlay />
    </div>
  );
}

export default Music;
