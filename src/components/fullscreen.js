import React, { Component } from "react";
import helpers from "../helper/helpers";

export default class FullScreen extends Component {
  componentDidMount() {
    helpers(this.props.width, this.props.height, false);
  }
  render() {
    let picSrc = require(`../assets/${this.props.src}`);
    return (
      <>
        <div id="container">
          <div className="page_content">
            <img src={picSrc} alt="photoAlbum"></img>
          </div>
        </div>
      </>
    );
  }
}
