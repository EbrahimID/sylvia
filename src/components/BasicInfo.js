import React, { Component } from "react";
import { Link } from "react-router-dom";
import Music from "./Music";
import helpers from "../helper/helpers";

export default class BasicInfo extends Component {
  componentDidMount() {
    helpers(915, 680);
  }

  render() {
    console.log("calling scaler");

    return (
      <>
        <div id="container" className="background">
          <div className="page_content">
            <img
              src={require("../assets/SixWeeksofChristmas.jpg")}
              alt="basic info"
            ></img>
            <Link to="/Teaser" className="Ellipse_1"></Link>
            <Music src="2-Index-SugarPlumFairy.mp3" />
          </div>
        </div>
      </>
    );
  }
}
