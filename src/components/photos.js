import React, { useState, useCallback } from "react";
import Music from "./Music";
import Gallery from "react-photo-gallery";
import Carousel, { Modal, ModalGateway } from "react-images";

export default function Photos() {
  const [currentImage, setCurrentImage] = useState(0);
  const [viewerIsOpen, setViewerIsOpen] = useState(false);

  const openLightbox = useCallback((event, { photo, index }) => {
    setCurrentImage(index);
    setViewerIsOpen(true);
  }, []);

  const closeLightbox = () => {
    setCurrentImage(0);
    setViewerIsOpen(false);
  };

  const photos = [
    {
      src: require("../assets/Photos/1_Easy-Resize.com.jpg"),
      width: 1280,
      height: 947
    },
    {
      src: require("../assets/Photos/2_Easy-Resize.com.jpg"),
      width: 1280,
      height: 911
    },
    {
      src: require("../assets/Photos/3_Easy-Resize.com.jpg"),
      width: 1280,
      height: 972
    },
    {
      src: require("../assets/Photos/4_Easy-Resize.com.jpg"),
      width: 1280,
      height: 981
    },
    {
      src: require("../assets/Photos/5_Easy-Resize.com.jpg"),
      width: 1280,
      height: 882
    },
    {
      src: require("../assets/Photos/6_Easy-Resize.com.jpg"),
      width: 1280,
      height: 885
    },
    {
      src: require("../assets/Photos/7_Easy-Resize.com.jpg"),
      width: 1280,
      height: 850
    },
    {
      src: require("../assets/Photos/8_Easy-Resize.com.jpg"),
      width: 1280,
      height: 886
    },
    {
      src: require("../assets/Photos/9_Easy-Resize.com.jpg"),
      width: 1280,
      height: 859
    },
    {
      src: require("../assets/Photos/10_Easy-Resize.com.jpg"),
      width: 1280,
      height: 832
    },
    {
      src: require("../assets/Photos/11_Easy-Resize.com.jpg"),
      width: 1280,
      height: 850
    },
    {
      src: require("../assets/Photos/12_Easy-Resize.com.jpg"),
      width: 1280,
      height: 851
    },
    {
      src: require("../assets/Photos/13_Easy-Resize.com.jpg"),
      width: 1280,
      height: 850
    },
    {
      src: require("../assets/Photos/14_Easy-Resize.com.jpg"),
      width: 1280,
      height: 895
    },
    {
      src: require("../assets/Photos/15_Easy-Resize.com.jpg"),
      width: 1280,
      height: 850
    },
    {
      src: require("../assets/Photos/16_Easy-Resize.com.jpg"),
      width: 1280,
      height: 850
    },
    {
      src: require("../assets/Photos/17_Easy-Resize.com.jpg"),
      width: 1280,
      height: 850
    },
    {
      src: require("../assets/Photos/18_Easy-Resize.com.jpg"),
      width: 1280,
      height: 855
    },
    {
      src: require("../assets/Photos/19_Easy-Resize.com.jpg"),
      width: 1280,
      height: 834
    },
    {
      src: require("../assets/Photos/20-min.JPG"),
      width: 1100,
      height: 768
    },
    {
      src: require("../assets/Photos/21-min.JPG"),
      width: 1100,
      height: 768
    },
    {
      src: require("../assets/Photos/22-min.JPG"),
      width: 1100,
      height: 768
    },
    {
      src: require("../assets/Photos/23-min.JPG"),
      width: 1100,
      height: 768
    },
    {
      src: require("../assets/Photos/24-min.JPG"),
      width: 1100,
      height: 768
    },
    {
      src: require("../assets/Photos/25-min.JPG"),
      width: 1100,
      height: 768
    },
    {
      src: require("../assets/Photos/26-min.JPG"),
      width: 1100,
      height: 768
    },
    {
      src: require("../assets/Photos/27-min.JPG"),
      width: 1100,
      height: 768
    },
    {
      src: require("../assets/Photos/28-min.JPG"),
      width: 1100,
      height: 768
    },
    {
      src: require("../assets/Photos/29-min.JPG"),
      width: 1100,
      height: 768
    },
    {
      src: require("../assets/Photos/30-min.JPG"),
      width: 4024,
      height: 3032
    },
    {
      src: require("../assets/Photos/31-min.JPG"),
      width: 3024,
      height: 4032
    },
    {
      src: require("../assets/Photos/32-min.JPG"),
      width: 4024,
      height: 3032
    },
    {
      src: require("../assets/Photos/33-min.JPG"),
      width: 3024,
      height: 4032
    },
    {
      src: require("../assets/Photos/34-min.JPG"),
      width: 3024,
      height: 4032
    },
    {
      src: require("../assets/Photos/35-min.JPG"),
      width: 3024,
      height: 4032
    },
    {
      src: require("../assets/Photos/35-min.JPG"),
      width: 3024,
      height: 4032
    },
    {
      src: require("../assets/Photos/36-min.JPG"),
      width: 3024,
      height: 4032
    },
    {
      src: require("../assets/Photos/37-min.JPG"),
      width: 4024,
      height: 3032
    },
    {
      src: require("../assets/Photos/38-min.JPG"),
      width: 4024,
      height: 3032
    },
    {
      src: require("../assets/Photos/39-min.JPG"),
      width: 3024,
      height: 4032
    },
    {
      src: require("../assets/Photos/40-min.JPG"),
      width: 3024,
      height: 4032
    },
    {
      src: require("../assets/Photos/41-min.JPG"),
      width: 3024,
      height: 4032
    },
    {
      src: require("../assets/Photos/42-min.JPG"),
      width: 3024,
      height: 4032
    },
    {
      src: require("../assets/Photos/43-min.JPG"),
      width: 3024,
      height: 4032
    },
    {
      src: require("../assets/Photos/44-min.JPG"),
      width: 3024,
      height: 4032
    },
    {
      src: require("../assets/Photos/45-min.JPG"),
      width: 3024,
      height: 4032
    },
    {
      src: require("../assets/Photos/46-min.JPG"),
      width: 3024,
      height: 4032
    },
    {
      src: require("../assets/Photos/47-min.JPG"),
      width: 3024,
      height: 4032
    },
    {
      src: require("../assets/Photos/48-min.JPG"),
      width: 3024,
      height: 4032
    },
    {
      src: require("../assets/Photos/49-min.JPG"),
      width: 3024,
      height: 4032
    },
    {
      src: require("../assets/Photos/50-min.JPG"),
      width: 3024,
      height: 4032
    },
    {
      src: require("../assets/Photos/51-min.JPG"),
      width: 3024,
      height: 4032
    }
  ];

  return (
    <div>
      <Gallery photos={photos} onClick={openLightbox} />
      <ModalGateway>
        {viewerIsOpen ? (
          <Modal onClose={closeLightbox}>
            <Carousel
              currentIndex={currentImage}
              views={photos.map(x => ({
                ...x,
                srcset: x.srcSet,
                caption: x.title
              }))}
            />
          </Modal>
        ) : null}
      </ModalGateway>
      <Music src="Andy.mp3" />
    </div>
  );
}
