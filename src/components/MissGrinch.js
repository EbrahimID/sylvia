import React, { Component } from "react";
import helpers from "../helper/helpers";

export default class MissGrinch extends Component {
  componentDidMount() {
    helpers(1920, 1080);
  }
  render() {
    return (
      <div id="container" style={{ overflow: "none" }}>
        <div className="page_content">
          <video
            id="video"
            src={require("../assets/Miss_Grinchpreview.mp4")}
            controls
            autoPlay
            loop
          />
        </div>
      </div>
    );
  }
}
