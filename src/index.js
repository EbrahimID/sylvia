import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import App from "./App";
import * as serviceWorker from "./serviceWorker";
import { BrowserRouter, Switch, Route } from "react-router-dom";
import InvitationDetail from "./components/InvitationDetail.js";
import EntertaintmentProgram from "./components/entertainmentProgram";
import BasicInfo from "./components/BasicInfo";
import MissGrinch from "./components/MissGrinch";
import Photos from "./components/photos";
import Teaser from "./components/Teaser";

ReactDOM.render(
  <BrowserRouter>
    <Switch>
      <Route exact path="/" component={BasicInfo} />
      <Route exact path="/BasicInfo" component={BasicInfo} />
      <Route exact path="/Teaser" component={Teaser} />
      <Route exact path="/InvitationDetail" component={InvitationDetail} />
      <Route exact path="/Photos" component={Photos} />
      <Route
        exact
        path="/EntertainmentProgram"
        component={EntertaintmentProgram}
      />
      <Route exact path="/MissGrinch" component={MissGrinch} />
    </Switch>
  </BrowserRouter>,
  document.getElementById("root")
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
